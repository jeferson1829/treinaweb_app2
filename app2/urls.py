from django.conf.urls import patterns, include, url
from django.contrib import admin
from artigos.feeds import ArtigosRss
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'artigos.views.index', {'pagina': 1}),
    url(r'^paginacao/(?P<pagina>[^\.]+)','artigos.views.index'),
    url(r'^artigo/(?P<url>[^\.]+)','artigos.views.artigo'),
    url(r'^form-pesquisa/$', 'artigos.views.form_pesquisa'),
    url(r'^pesquisa/$', 'artigos.views.pesquisa'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^contato','artigos.views.contato'),
    url(r'^rss/(?P<url>.*)', ArtigosRss()),
    url(r'^comments/', include('django_comments.urls')),
)

if settings.LOCAL:
    urlpatterns += patterns('',
        url(r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )