__author__ = 'stark'
from django import forms

class FormContato(forms.Form):
    nome = forms.CharField()
    email = forms.EmailField()
    telefone = forms.CharField(max_length=13)
    mensagem = forms.CharField(widget=forms.Textarea(), max_length=400)

