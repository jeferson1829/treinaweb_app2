from django.shortcuts import render, get_object_or_404
from artigos.models import Artigo
from django.core.paginator import Paginator
from artigos.forms import FormContato
from django.core.mail import send_mail
from django.conf import settings

def index(request, pagina=1):
    paginacao = Paginator(Artigo.objects.all(), 3)
    try:
        resumo = paginacao.page(pagina)
    except PageNotAnInteger:
        resumo = paginacao.page(1)
    except EmptyPage:
        resumo = paginacao.page(Paginator.num_pages)

    return render(request, 'index.html', {
        'artigos': resumo.object_list,
        'paginacao': resumo,
        'page_now': pagina,
    })


def artigo(request, url):
    return render(request, 'detail.html', {
        'artigo': get_object_or_404(Artigo, url=url),
        'media_url': settings.MEDIA_URL
    })


def form_pesquisa(request):
    return render(request, 'search_form.html')


def pesquisa(request):
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']
        artigos = Artigo.objects.filter(titulo__icontains=q) | Artigo.objects.filter(conteudo__icontains=q)
        return render(request, 'search_results.html',
                      {'artigos': artigos, 'query': q})
    else:
        return render(request, 'search_form.html', {'erro': True})


def contato(request):
    if request.method == "POST":
        form = FormContato(request.POST)
        if form.is_valid():
            email = ['mp_bra_nco@hotmail.com']
            rementente = form.cleaned_data['email']
            assunto = "CONTATO - " + form.cleaned_data['nome']
            mensagem = "Telefone: " + form.cleaned_data['telefone'] + "<br />" + form.cleaned_data['mensagem']
            send_mail(assunto, mensagem, rementente, email)

            return render(request, 'contact.html', {"form": FormContato(), "send": True})
    else:
        form = FormContato()

    return render(request, 'contact.html', {"form": form})