# coding=utf-8
from django.contrib import admin
from artigos.models import Agencia, Autor, Artigo

class AutorAdmin(admin.ModelAdmin):
    list_display = ('nome', 'email')
    search_fields = ('nome', 'email')
    ordering = ('nome',)

class ArtigoAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'pub_date', 'agencia')
    search_fields = ('titulo', 'agencia')
    list_filter = ('pub_date',)
    date_hierarchy = 'pub_date'
    ordering = ('-pub_date',)
    fields = ('titulo', 'url', 'pub_date', 'autores', 'agencia', 'imagemDestaque', 'conteudo') #Alterado porque não consigo subir as imagens..
    #fields = ('titulo', 'url', 'pub_date', 'autores', 'agencia', 'conteudo')
    prepopulated_fields = {'url': ('titulo',)}
    filter_horizontal = ('autores',)

class AgenciaAdmin(admin.ModelAdmin):
    list_display = ('nome', 'site')
    search_fields = ('nome', 'site')
    ordering = ('nome',)

admin.site.register(Agencia, AgenciaAdmin)
admin.site.register(Autor, AutorAdmin)
admin.site.register(Artigo, ArtigoAdmin)

