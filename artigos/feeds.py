from django.contrib.syndication.views import Feed
from artigos.models import Artigo

class ArtigosRss(Feed):
    title = 'Ultimos artigos do site'
    link = '/'
    description = 'Ultimos artigos do site de teste do Curso Django Treina Web'

    def items(self):
        return Artigo.objects.all()

    def item_link(self, artigo):
        return '/artigo/%s' % artigo.url

    def item_description(self, artigo):
        return artigo.conteudo

    def item_pubdate(self, artigo):
        return artigo.pub_date

